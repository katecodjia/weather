import 'package:flutter/material.dart';
import 'package:weather/timer.dart';
import 'package:weather_repository/weather_repository.dart';

import 'app.dart';
class AllPage extends StatelessWidget {
  const AllPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Center(
        child:  Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextButton.icon(
              onPressed: (){
                Navigator.of(context).push(
                    PageRouteBuilder(
                        transitionDuration: const Duration(milliseconds: 650),
                        pageBuilder: (context,animation, _){
                          return FadeTransition(
                            opacity: animation,
                            child: WeatherApp(weatherRepository: WeatherRepository()),
                          );
                        }
                    )
                );
              },
              label: const Text(
                "Recipes App",
                style: TextStyle(fontSize: 20),
              ), icon: const Icon(Icons.food_bank,size: 22,),),
            TextButton.icon(
              onPressed: (){
                Navigator.of(context).push(
                    PageRouteBuilder(
                        transitionDuration: const Duration(milliseconds: 650),
                        pageBuilder: (context,animation, _){
                          return FadeTransition(
                            opacity: animation,
                            child: const AppTimer()
                          );
                        }
                    )
                );
              },
              label: const Text(
                "Timer",
                style: TextStyle(fontSize: 20),
              ), icon: const Icon(Icons.food_bank,size: 22,),),
          ],
        ),
      ),
    );
  }
}
