import 'package:flutter/material.dart';
import 'package:weather/widgets/time.dart';
class CustomModal extends StatefulWidget {
  const CustomModal({Key? key}) : super(key: key);

  @override
  State<CustomModal> createState() => _CustomModalState();
}

class _CustomModalState extends State<CustomModal> {
  var data = [
    {'time': '09:00 AM', 'temp': '19°'},
    {'time': '10:00 AM', 'temp': '20°'},
    {'time': '11:00 AM', 'temp': '21°'},
    {'time': '12:00 AM', 'temp': '18°'},
    {'time': '13:00 AM', 'temp': '17°'},
    {'time': '14:00 AM', 'temp': '16°'},
    {'time': '15:00 AM', 'temp': '15°'},
    {'time': '16:00 AM', 'temp': '11°'},
    {'time': '17:00 AM', 'temp': '20°'},
    {'time': '18:00 AM', 'temp': '19°'},
  ];
   int selectIndex = 1;
  @override
  Widget build(BuildContext context) {
    double res_width = MediaQuery.of(context).size.width;
    double res_height = MediaQuery.of(context).size.height;

    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(50),
            topRight: Radius.circular(50)),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            SizedBox(
              height: res_height * 0.04,
            ),
            Row(
              children: [
                Image.asset(
                  "assets/images/clock.png",
                  width: 25,
                ),
                SizedBox(
                  width: res_width * 0.04,
                ),
                const Text(
                  "Today's changes",
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600),
                )
              ],
            ),
            SizedBox(
              height: res_height * 0.04,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    CircleAvatar(
                      backgroundColor: const Color(0xff763bd7)
                          .withOpacity(0.1),
                      radius: 25,
                      child: Image.asset(
                        "assets/images/rainy.png",
                        width: 25,
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    const Text(
                      "Rainy with\nshort storms ",
                      style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w600),
                    )
                  ],
                ),
                Row(
                  children: [
                    CircleAvatar(
                      backgroundColor: const Color(0xffff5580)
                          .withOpacity(0.1),
                      radius: 25,
                      child: Image.asset(
                        "assets/images/wind.png",
                        width: 25,
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    const Text(
                      "Wind EN\n8 km/h",
                      style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w600),
                    )
                  ],
                ),
              ],
            ),
            SizedBox(
              height: res_height * 0.04,
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: data.length,
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        const SizedBox(
                          height: 5,
                        ),
                        TimeWidget(
                          data: data[index],
                          active: selectIndex == index,
                          onTap: (){
                            setState(() {
                              selectIndex = index;
                            });
                          },
                        ),
                        const Divider(
                          color: Colors.grey,
                        )
                      ],
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}

