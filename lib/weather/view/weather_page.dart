import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_repository/weather_repository.dart';

import '../../home_page.dart';
import '../../search/view/search_page.dart';
import '../../settings/view/settings_page.dart';
import '../../theme/cubit/theme_cubit.dart';
import '../../widgets/modal.dart';
import '../cubit/weather_cubit.dart';
import '../widgets/weather_empty.dart';
import '../widgets/weather_error.dart';
import '../widgets/weather_loading.dart';

class WeatherPage extends StatelessWidget {
  const WeatherPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => WeatherCubit(context.read<WeatherRepository>()),
      child: const WeatherView(),
    );
  }
}

class WeatherView extends StatefulWidget {
  const WeatherView({super.key});

  @override
  State<WeatherView> createState() => _WeatherViewState();
}

class _WeatherViewState extends State<WeatherView> {
  @override
  Widget build(BuildContext context) {
    bool showdate=true;
    return Scaffold(
      body: Center(
        child: BlocConsumer<WeatherCubit, WeatherState>(
          listener: (context, state) {
            if (state.status.isSuccess) {
              context.read<ThemeCubit>().updateTheme(state.weather);
            }
          },
          builder: (context, state) {
            switch (state.status) {
              case WeatherStatus.initial:
                return const WeatherEmpty();
              case WeatherStatus.loading:
                return const WeatherLoading();
              case WeatherStatus.success:
                return MyHomePage(
                  weather: state.weather,
                  units: state.temperatureUnits,
                  onRefresh: () {
                    return context.read<WeatherCubit>().refreshWeather();
                  }, showdate: showdate,
                );
              case WeatherStatus.failure:
                return const WeatherError();
            }
          },
        ),
      ),
      bottomNavigationBar: Container(
        decoration: const BoxDecoration(color: Color(0xff763bd7)),
        child: ClipRRect(
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(24),
            topRight: Radius.circular(24),
          ),
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 13, right: 13, top: 13, bottom: 26),
              child: ListTile(
                leading:IconButton(
                  icon: const Icon(Icons.settings,size: 30,),
                  onPressed: () {
                    Navigator.of(context).push<void>(
                      SettingsPage.route(
                        context.read<WeatherCubit>(),
                      ),
                    );
                  },
                ),
                title: InkWell(
                  onTap: () {
                    setState(() {
                      showdate = true;
                    });
                    showModalBottomSheet(
                        context: context,
                        elevation: 0,
                        backgroundColor: Colors.transparent,
                        builder: (builder) {
                          return const SizedBox(
                            height: 600,
                            child: CustomModal(),
                          );
                        }).whenComplete(() {
                      setState(() {
                        showdate = false;
                      });
                    });
                  },
                  child: const Text(
                    "Today's Changes",
                    style: TextStyle(
                        fontWeight: FontWeight.w500, fontSize: 20),
                  ),
                ),
                trailing: IconButton(
              onPressed: () async {
                final city = await Navigator.of(context).push(SearchPage.route());
                if (!mounted) return;
                await context.read<WeatherCubit>().fetchWeather(city);
              }, icon: const Icon(Icons.search,size: 30,color:  Color(0xffff5580),),
                  
                )
              ),
            ),
          ),
        ),
      ),
    );
  }
}
