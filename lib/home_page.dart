import 'package:flutter/material.dart';
import 'package:weather/weather/models/weather.dart';
import 'dart:math' as math;
import 'package:weather/widgets/rain.dart';
import 'package:weather_repository/weather_repository.dart';

class MyHomePage extends StatefulWidget {
   MyHomePage({
    super.key,
    required this.weather,
    required this.units,
    required this.onRefresh,
     required this.showdate,
  });
  final MyWeather weather;
  final TemperatureUnits units;
  final ValueGetter<Future<void>> onRefresh;
  bool showdate = true;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final Key _key = GlobalKey();
  List<String> daysOfWeek = [
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat",
    "Sun"
  ];

  @override
  Widget build(BuildContext context) {
    double res_width = MediaQuery.of(context).size.width;
    double res_height = MediaQuery.of(context).size.height;
    return Scaffold(

        body: RefreshIndicator(
          onRefresh: widget.onRefresh,
          child: Container(
            height: res_height * 1,
            color: const Color(0xFF763bd7),
            child: Stack(
              children: [
                AnimatedContainer(
                  duration: const Duration(milliseconds: 600),
                  height: widget.showdate ? res_height * 0.15 : res_width * 0.25,
                  width: double.infinity,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius:
                      BorderRadius.only(bottomLeft: Radius.circular(50))),
                  child: Padding(
                    padding: EdgeInsets.only(left: 20, top: res_height * 0.01),
                    child: Row(
                      children: [
                        _WeatherIcon(condition: widget.weather.condition),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  right: 0,
                  child: ClipRRect(
                    borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(50)),
                    child: Column(
                      children: [
                        Container(
                          width: 70,
                          height: 120,
                          color: const Color(0xffff5580),
                          child: const Icon(Icons.menu,
                              color: Colors.white, size: 30),
                        ),
                        AnimatedContainer(
                          color: const Color(0xffff5580),
                          width: 70,
                          height: widget.showdate ? 110 : 0,
                          duration: const Duration(milliseconds: 400),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children:  [
                              Text(
                                "${daysOfWeek[widget.weather.lastUpdated.weekday - 1]},",

                                style: const TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                              Text(
                                widget.weather.lastUpdated.day.toString(),
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                widget.weather.condition.name =="rainy"? Transform.rotate(
                  angle: math.pi / 4,
                  child: SizedBox(
                    height: res_height * 1,
                    child: ParallaxRain(
                      key: _key,
                      dropColors: const [Colors.white],
                      trail: true,
                    ),
                  ),
                ):const SizedBox.shrink(),
                Positioned(
                    right: 0,
                    left: 20,
                    top: res_height * 0.275,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          widget.weather.formattedTemperature(widget.units),
                          style: const TextStyle(
                              color: Colors.white, fontSize: 100,fontWeight: FontWeight.bold),
                        ),
                        Text(
                          widget.weather.degree(widget.units),
                          style: const TextStyle(
                              color: Colors.white, fontSize: 70),
                        ),

                        Expanded(child: _WeatherImage(condition: widget.weather.condition,),)


                      ],
                    )
                ),
                Positioned(
                    left: 20,
                    top: res_height * 0.200,
                    child: Column(
                      children: [
                        Text(
                          widget.weather.location,
                          style: const TextStyle(color: Colors.white, fontSize: 40),
                        ),
                        const SizedBox(
                          height: 10,
                        )
                      ],
                    )),
                Positioned(
                  bottom: res_height * 0.05,
                  left: 20,
                  child: Text(
                    '''Updated at ${TimeOfDay.fromDateTime(widget.weather.lastUpdated).format(context)}''',
                    style: const TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}

class _WeatherIcon extends StatelessWidget {
  const _WeatherIcon({required this.condition});

  static const _iconSize = 50.0;

  final WeatherCondition condition;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
      Text(
      condition.toEmoji,
      style: const TextStyle(fontSize: _iconSize),
    ),
        const SizedBox(width: 10,),
        Text(
          condition.name.substring(0, 1).toUpperCase() + condition.name.substring(1),
            style: const TextStyle(fontSize: 24)    ),
      ],
    );

  }
}

class _WeatherImage extends StatelessWidget {
  const _WeatherImage({required this.condition});

  final WeatherCondition condition;

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      "assets/images/${condition.toImage}",
      height:  MediaQuery.of(context).size.height * 0.5,
    );

  }
}

extension on WeatherCondition {
  String get toEmoji {
    switch (this) {
      case WeatherCondition.clear:
        return '☀️';
      case WeatherCondition.rainy:
        return '🌧️';
      case WeatherCondition.cloudy:
        return '☁️';
      case WeatherCondition.snowy:
        return '🌨️';
      case WeatherCondition.unknown:
        return '❓';
    }
  }
}
extension on WeatherCondition {
  String get toImage {
    switch (this) {
      case WeatherCondition.clear:
        return 'sun.png';
      case WeatherCondition.rainy:
        return 'girl.png';
      case WeatherCondition.cloudy:
        return 'cloudy.png';
      case WeatherCondition.snowy:
        return 'snowy.png';
      case WeatherCondition.unknown:
        return '❓';
    }
  }
}

class _WeatherBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final color = Theme.of(context).primaryColor;
    return SizedBox.expand(
      child: DecoratedBox(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: const [0.25, 0.75, 0.90, 1.0],
            colors: [
              color,
              color.brighten(),
              color.brighten(33),
              color.brighten(50),
            ],
          ),
        ),
      ),
    );
  }
}

extension on Color {
  Color brighten([int percent = 10]) {
    assert(
      1 <= percent && percent <= 100,
      'percentage must be between 1 and 100',
    );
    final p = percent / 100;
    return Color.fromARGB(
      alpha,
      red + ((255 - red) * p).round(),
      green + ((255 - green) * p).round(),
      blue + ((255 - blue) * p).round(),
    );
  }
}

extension on MyWeather {
  String formattedTemperature(TemperatureUnits units) {
    return '''${temperature.value.toStringAsPrecision(2)}°''';
  }
}
extension on MyWeather {
  String degree(TemperatureUnits units) {
    return units.isCelsius ? 'C' : 'F';
  }
}