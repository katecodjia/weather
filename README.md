# weather
A small flutter application that displays the weather according to the selected city

## Getting started

```
git clone https://gitlab.com/katecodjia/weather.git
flutter clean
flutter packages get
flutter run
```

## App Preview
|                   Screen 1                    |                          Screen 2                           |                        Screen 3                         |
|:---------------------------------------------:|:-----------------------------------------------------------:|:-------------------------------------------------------:|
|      <img src="screenshots/home.png" alt="home" width="350">      | <img src="screenshots/search.png" alt="search" width="350"> |          <img src="screenshots/setting.png" alt="search" width="350">          |

|                      Demo                      |
|:----------------------------------------------:|
| <img src="screenshots/app_demo.gif" width="350"> |